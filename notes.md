## Creating a new git project in a new git repo

1. Login to gitlab.com
2. Click "Create" a new project
3. Specify Project title & path 
4. Copy git repo URL

5. Create new local folder on your PC
6. cd [folder]
7. Run the following command 
	git clone [repo URL]

8. Add some files to the new git folder
9. Run the following commands:
	git add .
	git commit -m "Add some comment"
	git push

10. Chill =)


## Sending existing local project to a new GIT repo

1. Create some local folder with some files inside on your PC

2. Login to gitlab.com
3. Click create a new project
4. Specify Project title & path
5. Copy GIT repo URL

6. cd [folder]
7. Run the following commands:
	git init
	git remote add origin [repo URL]

8. Make some changes with files

9. Run the following commands:
	git add .
	git commit -m "Add some comment"
	git push --set-upstream origin master


 
	
	
